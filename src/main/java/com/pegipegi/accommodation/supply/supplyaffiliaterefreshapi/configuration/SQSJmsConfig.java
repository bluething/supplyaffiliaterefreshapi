package com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;


@Configuration
@EnableJms
public class SQSJmsConfig {

	private SQSConnectionFactory connectionFactory;
	
	public SQSJmsConfig(@Value("${aws.access-key}") String awsAccessKey,
            @Value("${aws.secret-key}") String awsSecretKey,
            @Value("${aws.sqs.endpoint}") String endPoint) {
		ProviderConfiguration providerConfiguration = new ProviderConfiguration();
		BasicAWSCredentials basicCredential = new BasicAWSCredentials("X", "X");
		AmazonSQS sqs = AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(basicCredential))
				.withEndpointConfiguration(new EndpointConfiguration(endPoint, ""))
				.build();
		connectionFactory = new SQSConnectionFactory(providerConfiguration, sqs);
	}
	
	@Bean
	public SQSConnectionFactory sqsConnectionFactory(@Value("${aws.access-key}") String awsAccessKey,
            @Value("${aws.secret-key}") String awsSecretKey,
            @Value("${aws.sqs.endpoint}") String endPoint) {

		ProviderConfiguration providerConfiguration = new ProviderConfiguration();
		BasicAWSCredentials basicCredential = new BasicAWSCredentials("X", "X");
		AmazonSQS sqs = AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(basicCredential))
				.withEndpointConfiguration(new EndpointConfiguration(endPoint, ""))
				.build();
		return new SQSConnectionFactory(providerConfiguration, sqs);
	
	}
	
	@Bean
    public JmsTemplate defaultJmsTemplate() {
        return new JmsTemplate(this.connectionFactory);
    }
	
}
