package com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.refreshari.service;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;

import com.amazon.sqs.javamessaging.SQSMessagingClientConstants;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.base.Publisher;
import com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.refreshari.model.RefreshmentRequest;

@Service
public final class RefreshmentService implements Publisher<RefreshmentRequest> {

	private JmsTemplate jmsTemplate;

	@Autowired
	public RefreshmentService(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;	
	}

	@Override
	public void sent(RefreshmentRequest refreshmentRequest) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		String message = mapper.writeValueAsString(refreshmentRequest);
		
		jmsTemplate.send("sampleQueue.fifo", new MessageCreator() {
			@Override
			public Message createMessage(Session session) throws JMSException {
				TextMessage textMessage = session.createTextMessage(message);
				textMessage.setStringProperty(SQSMessagingClientConstants.JMSX_GROUP_ID, "messageGroup1");
				textMessage.setStringProperty(SQSMessagingClientConstants.JMS_SQS_DEDUPLICATION_ID, "1" + System.currentTimeMillis());
				textMessage.setStringProperty("documentType", refreshmentRequest.getClass().getName());
				return textMessage;
			}
		});
	}

}
