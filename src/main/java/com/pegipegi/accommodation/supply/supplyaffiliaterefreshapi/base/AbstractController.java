package com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.base;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class AbstractController<Request, Response> {
	
	protected abstract void validate(Request request);
	protected abstract Response execute(Request request) throws Exception;
	protected abstract ResponseEntity<Response> buildResponse(Request request) throws Exception;
	
	protected ResponseEntity<Response> getResponseEntity(Request request) throws Exception{
		
		validate(request);
		
		Response responseObject = execute(request);
		return new ResponseEntity<Response>(responseObject, HttpStatus.OK);
	}

}
