package com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.base;

public interface Publisher<T> {

	public void sent(T t) throws Exception;

}
