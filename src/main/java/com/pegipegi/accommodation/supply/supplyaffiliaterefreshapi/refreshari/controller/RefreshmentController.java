package com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.refreshari.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.base.AbstractController;
import com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.base.Publisher;
import com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.refreshari.model.RefreshmentRequest;
import com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.refreshari.model.RefreshmentResponse;

@RestController
public final class RefreshmentController extends AbstractController<RefreshmentRequest, RefreshmentResponse>{
	
	private Publisher<RefreshmentRequest> publisher;
	
	@Autowired
	public RefreshmentController(@Qualifier("refreshmentService") Publisher<RefreshmentRequest> publisher) {
		this.publisher = publisher;
	}

	@Override
	protected void validate(RefreshmentRequest request) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected RefreshmentResponse execute(RefreshmentRequest request) throws Exception {
		publisher.sent(request);
		return new RefreshmentResponse("ok");
	}

	@Override
	@RequestMapping(value = "/refreshment", method = RequestMethod.POST, produces = "application/json")
	protected ResponseEntity<RefreshmentResponse> buildResponse(@RequestBody RefreshmentRequest request) throws Exception {
		ResponseEntity<RefreshmentResponse> entity = getResponseEntity(request);
		return entity;
	}

}
