package com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.refreshari.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
public final class RefreshmentResponse {
	
	private String status;

}
