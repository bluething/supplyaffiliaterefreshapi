package com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.refreshari.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;

@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public final class RefreshmentRequest {
	
	private String id;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date checkinDate;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date checkOutDate;
	private List<String> hotelCanonIdList;
	
	public RefreshmentRequest(String id, Date checkinDate, Date checkOutDate, List<String> hotelCanonIdList) {
		this.id = id;
		this.checkinDate = checkinDate;
		this.checkOutDate = checkOutDate;
		this.hotelCanonIdList = hotelCanonIdList;
	}

}
