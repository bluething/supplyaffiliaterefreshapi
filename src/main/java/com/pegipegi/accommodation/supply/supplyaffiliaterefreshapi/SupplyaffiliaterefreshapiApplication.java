package com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupplyaffiliaterefreshapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SupplyaffiliaterefreshapiApplication.class, args);
	}

}
