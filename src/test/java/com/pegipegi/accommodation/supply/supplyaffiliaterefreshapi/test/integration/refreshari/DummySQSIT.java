package com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.test.integration.refreshari;

import org.elasticmq.rest.sqs.SQSRestServer;
import org.elasticmq.rest.sqs.SQSRestServerBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.test.integration.IntegrationTest;

/*
 * Alias for the SpringJUnit4ClassRunner. It is a custom extension of JUnit’s BlockJUnit4ClassRunner.
 * Used to provide a bridge between Spring Boot test features and JUnit. 
 * Whenever we are using any Spring Boot testing features in out JUnit tests, 
 * this annotation will be required.
 * */
@RunWith(SpringRunner.class)
/*
 * This annotation works by creating the ApplicationContext used in our tests 
 * through SpringApplication. It starts the embedded server, creates a web environment 
 * and then enables @Test methods to do integration testing.
 * */
@SpringBootTest
/*
 * Annotation that can be applied to a test class to enable and configure auto-configuration of MockMvc.
 * */
@AutoConfigureMockMvc
/*
 * Test annotation which indicates that the ApplicationContext associated with a test is dirty 
 * and should therefore be closed and removed from the context cache.
 * So it will forces the Application Context (and SQS connections) to reset between tests.
 * ElasticMQ will fail to stop if there are active connections.
 * */
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Category(IntegrationTest.class)
public class DummySQSIT {
	
	private static SQSRestServer sqsRestServer;
	private static String endPoint;
	private static AmazonSQS sqsClient;
	private static String queueUrl;
	
	// pay attention to this!
	@BeforeClass
	public static void setup() {
		// creates an Elastic MQ Server on a port 40000
		sqsRestServer = SQSRestServerBuilder
				.withInterface("0.0.0.0")
				.withPort(40000)
				.start();
		// creates sqs endpoint
		endPoint = "http://localhost:" + sqsRestServer.waitUntilStarted().localAddress().getPort();
		// An SQS client for operating on queues outside of JMS listeners, etc. in app
		BasicAWSCredentials basicCredential = new BasicAWSCredentials("X", "X");
		sqsClient = AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(basicCredential))
				.withEndpointConfiguration(new EndpointConfiguration(endPoint, ""))
				.build();
		// creates the queue before the Spring Application Context loads.
		queueUrl = sqsClient.createQueue("sampleQueue").getQueueUrl();

	}

	@AfterClass
	public static void cleanUp() {
		// delete queue
		sqsClient.deleteQueue(queueUrl);
		// wait the server to die
		sqsRestServer.stopAndWait();
	}

	@TestConfiguration
	static class TestQueueConfiguration {
		// pay attention to this!
		@Bean
		public SQSConnectionFactory sqsConnectionFactory() {
			ProviderConfiguration providerConfiguration = new ProviderConfiguration();
			BasicAWSCredentials basicCredential = new BasicAWSCredentials("X", "X");
			AmazonSQS sqs = AmazonSQSClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(basicCredential))
					.withEndpointConfiguration(new EndpointConfiguration(endPoint, ""))
					.build();
			return new SQSConnectionFactory(providerConfiguration, sqs);
		}
	}
	
	@Test
	public void dummyTest() {
		
	}
}
