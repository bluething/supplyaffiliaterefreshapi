package com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.test.integration;

import org.junit.experimental.categories.Categories.IncludeCategory;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.test.integration.refreshari.DummySQSIT;
import com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.test.integration.refreshari.RefreshmentIT;

@RunWith(Suite.class)
@IncludeCategory(IntegrationTest.class)
@SuiteClasses({RefreshmentIT.class, DummySQSIT.class})
public class IntegrationTestSuite {

}
