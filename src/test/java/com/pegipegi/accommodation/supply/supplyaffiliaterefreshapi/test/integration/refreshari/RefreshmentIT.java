package com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.test.integration.refreshari;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.elasticmq.rest.sqs.SQSRestServer;
import org.elasticmq.rest.sqs.SQSRestServerBuilder;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.amazon.sqs.javamessaging.ProviderConfiguration;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.refreshari.model.RefreshmentRequest;
import com.pegipegi.accommodation.supply.supplyaffiliaterefreshapi.test.integration.IntegrationTest;

/*
 * Alias for the SpringJUnit4ClassRunner. It is a custom extension of JUnit’s BlockJUnit4ClassRunner.
 * Used to provide a bridge between Spring Boot test features and JUnit. 
 * Whenever we are using any Spring Boot testing features in out JUnit tests, 
 * this annotation will be required.
 * */
@RunWith(SpringRunner.class)
/*
 * This annotation works by creating the ApplicationContext used in our tests 
 * through SpringApplication. It starts the embedded server, creates a web environment 
 * and then enables @Test methods to do integration testing.
 * */
@SpringBootTest
/*
 * Annotation that can be applied to a test class to enable and configure auto-configuration of MockMvc.
 * */
@AutoConfigureMockMvc
/*
 * Test annotation which indicates that the ApplicationContext associated with a test is dirty 
 * and should therefore be closed and removed from the context cache.
 * So it will forces the Application Context (and SQS connections) to reset between tests.
 * ElasticMQ will fail to stop if there are active connections.
 * */
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Category(IntegrationTest.class)
public class RefreshmentIT {

	private static SQSRestServer sqsRestServer;
	private static String endPoint;
	private static AmazonSQS sqsClient;
	private static String queueUrl;

	@Autowired
	private MockMvc mockMvc;

	// pay attention to this!
	@BeforeClass
	public static void setup() {
		// creates an Elastic MQ Server on a port 40000
		sqsRestServer = SQSRestServerBuilder
				.withInterface("0.0.0.0")
				.withPort(40000)
				.start();
		
		// creates sqs endpoint
		endPoint = "http://localhost:" + sqsRestServer.waitUntilStarted().localAddress().getPort();
		
		// An SQS client for operating on queues outside of JMS listeners, etc. in app
		BasicAWSCredentials basicCredential = new BasicAWSCredentials("X", "X");
		sqsClient = AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(basicCredential))
				.withEndpointConfiguration(new EndpointConfiguration(endPoint, ""))
				.build();
		
		Map<String, String> attributeMap = new HashMap<>();
		// A FIFO queue must have the FifoQueue attribute set to true.
		attributeMap.put("FifoQueue", "true");
		 /*
         * If the user doesn't provide a MessageDeduplicationId, generate a
         * MessageDeduplicationId based on the content.
         */
		attributeMap.put("ContentBasedDeduplication", "true");
        
		CreateQueueRequest createQueueRequest = new CreateQueueRequest("sampleQueue.fifo").withAttributes(attributeMap);
		
		// creates the queue before the Spring Application Context loads.
		queueUrl = sqsClient.createQueue(createQueueRequest).getQueueUrl();

	}

	@AfterClass
	public static void cleanUp() {
		// delete queue
		sqsClient.deleteQueue(queueUrl);
		// wait the server to die
		sqsRestServer.stopAndWait();
	}

	/*
	 * uncomment this if you want to use overriding bean instead of profile
	 * */
//	@TestConfiguration
//	static class TestQueueConfiguration {
//		// pay attention to this!
//		@Bean
//		public SQSConnectionFactory sqsConnectionFactory() {
//			ProviderConfiguration providerConfiguration = new ProviderConfiguration();
//			BasicAWSCredentials basicCredential = new BasicAWSCredentials("X", "X");
//			AmazonSQS sqs = AmazonSQSClientBuilder.standard()
//					.withCredentials(new AWSStaticCredentialsProvider(basicCredential))
//					.withEndpointConfiguration(new EndpointConfiguration(endPoint, ""))
//					.build();
//			return new SQSConnectionFactory(providerConfiguration, sqs);
//		}
//	}

	@Test
	public void refresh() throws Exception {

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

		//given
		List<String> hotelCanonIds = new ArrayList<>();
		hotelCanonIds.add("999999");
		hotelCanonIds.add("888888");

		RefreshmentRequest request = new RefreshmentRequest("abc-xyz", format.parse("2019-09-01"), format.parse("2019-09-02"), hotelCanonIds);

		ObjectMapper mapper = new ObjectMapper();

		//when
		String uri = "/refreshment";
		this.mockMvc.perform(MockMvcRequestBuilders.
				post(uri).
				contentType(MediaType.APPLICATION_JSON).
				accept(MediaType.APPLICATION_JSON).
				content(mapper.writeValueAsString(request))
				);
		
		//then
		AmazonSQS client = AmazonSQSClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("X", "X")))
				.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(endPoint, "elasticmq"))
				.build();
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl)
				.withMaxNumberOfMessages(1)
				.withWaitTimeSeconds(3);
		List<Message> messages = client.receiveMessage(receiveMessageRequest).getMessages();
		
		assertThat(mapper.writeValueAsString(request), equalTo(messages.get(0).getBody()));

	}

}
